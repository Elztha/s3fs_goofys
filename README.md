# Easy Using S3 with S3FS & Goofys
## Overview


**[s3fs](https://github.com/s3fs-fuse/s3fs-fuse)** allows Linux and macOS to mount an S3 bucket via FUSE. s3fs preserves the native object format for files, allowing use of other tools like AWS CLI.


**[Goofys](https://github.com/kahing/goofys)** allows you to mount an S3 bucket as a filey system.
It's a Filey System instead of a File System because goofys strives for performance first and POSIX second. Particularly things that are difficult to support on S3 or would translate into more than one round-trip would either fail (random writes) or faked (no per-file permission). Goofys does not have an on disk data cache (checkout catfs), and consistency model is close-to-open.

<!--**[AWS Storage Gateway](https://aws.amazon.com/storagegateway/)** is a hybrid cloud storage service that gives you on-premises access to virtually unlimited cloud storage. Customers use Storage Gateway to simplify storage management and reduce costs for key hybrid cloud storage use cases. These include moving tape backups to the cloud, reducing on-premises storage with cloud-backed file shares, providing low latency access to data in AWS for on-premises applications, as well as various migration, archiving, processing, and disaster recovery use cases.  -->

## Scenario

In this lab, we will mount our S3 Bucket to an EC2 Instances using multiple methods.

First we will use s3fs, we will create a **VPC** with **EC2** infrastructure through **Cloudformation**, and then try to connect to the **EC2 instance**. After that we will launch an **S3 Bucket** and try to mount the bucket to our EC2 Instance.

Second we will use goofys, we will use the same **VPC, EC2** and **S3 Bucket** and then mount the bucket to our EC2 Instance and see the differences of both system.

Then we will try to learn the differences between S3FS & Goofys through a short analysis. At last, we will try to implement a real life usecase of them by migrating media data in a wordpress site to save cost and increase scalability.

## Prerequisites

* Make sure your have an AWS Account
* Download the source file of this lab:
    * [NFSTutorial.yaml](./materials/NFSTutorial.yaml)

## Table of Contents


1. [Part 1 : S3FS](#s3fs)

	In this part, We will install s3fs and mount S3 Bucket using S3FS

2. [Part 2 : Goofys](#goofys)

	In this part, We will install goofys and mount our S3 Bucket using goofys

3. [Part 3 : S3FS vs Goofys](#analysis)
	In this part, We do a short analysis on the difference between s3fs and goofys to understand each advantages and disadvantages.

4. [Part 4 : Real Usecase Implementation](implement.md)

	On this last part, we will walk through a real usecase of both s3fs and goofys. 

## Step by step

<a name="s3fs"></a>
###1. Mount your S3 (s3fs)
	
<p align="center">
    <img src="./images/goofys.png" width=50% height=70%>
</p>

### Setting up your VPC & EC2 Environtment

> ***Important***

> We will make our Environment on the **N. Virginia** Region so make sure that you are already at the **N. Virginia** Region

Use the [CloudFormation template](./materials/VPC_lab1.yaml) to setup your VPC architecture

- On the **Service** menu, select [CloudFormation](https://console.aws.amazon.com/cloudformation/).

- Select **Create Stack**.

- ☑ **Template is ready**.

- In **Specify template**, ☑ **Upload a template file**.

- Select **Choose file**, and select **[NFSTutorial.yaml](./materials/NFSTutorial.yaml)** and click **Next**.


<p align="center">
    <img src="./images/1.png" width=70% height=70%>
</p>


- Input `NFSLab-Stack-1-your-name` for the **Stack name**

> Ex: `NFSLab-Stack-1-John`.

<p align="center">
    <img src="./images/2.png" width=70% height=70%>
</p>

- Choose the key file that you will use for the EC2 Instance & select **Next**.

- Leave the settings as default, select **Next**.

- Review the details of this stack, if there is no problem select **Create stack**.

### Create S3 bucket
- On the **service** menu, click **S3**.

- Click **Create Bucket**.

- For Bucket Name, type a **nfslab-stack-s3fs-your-name**.

> Ex: `nfslab-stack-s3fs-alvin`.

- For Region, choose **US East(N.Virginia)**.

<p align="center">
    <img src="./images/9.png" width=70% height=70%>
</p>


- Scroll down and click on **Create Bucket**

### Create an IAM Role to access the S3 Bucket

- On the **Service** menu, select **IAM**.

- In the navigation pane, click **Roles**.

- Click **Create policy**, then click on the **JSON** tab

- Copy and paste the IAM Policy JSON attached on [IAMPOLICY.json](./materials/IAMPOLICY.json)
> change the `{bucket-name}` part to your **nfslab-stack-s3fs-your-name** (S3 Bucket name).


<p align="center">
    <img src="./images/5.png" width=70% height=70%>
</p>

- Then, click on review, type in `s3fs-access` as the name of policy then add `Access to S3 Bucket` for the description and them click on **Create Policy**

<p align="center">
    <img src="./images/4.png" width=70% height=70%>
</p>

- Then in the navigation pane, click **Roles**, then click on **Create Roles**

- Pick **EC2** as the use case then click on **Next:Permissions**
<p align="center">
    <img src="./images/6.png" width=70% height=70%>
</p>

- On the **Filter Policies**, search for the **s3fs-access** & **AmazonSSMManagedInstanceCore** policy we just created and tick on it to attach

<p align="center">
    <img src="./images/7.png" width=70% height=70%>
</p>

<p align="center">
    <img src="./images/12.png" width=70% height=70%>
</p>

- Click on **Next:tags**, then click again on **Next:Review** and input your role name as `s3fs-ec2` and then click on **Create Role**

<p align="center">
    <img src="./images/8.png" width=70% height=70%>
</p>


### Attaching IAM Role to your EC2

- On the **Service** menu, select **EC2**.

- On the navigation bar, click on **Instances**

- **Add filter** for `NFS` to see your running instance, and then **right click**, choose **Instance Settings**, and then **Attach/Replace IAM Roles**

<p align="center">
    <img src="./images/10.png" width=70% height=70%>
</p>

- Choose the IAM role we created before (**s3fs-ec2**), then click **Apply**

<p align="center">
    <img src="./images/11.png" width=70% height=70%>
</p>


### Mounting your S3 Bucket to EC2

- On the **Service** menu, select **EC2**.

- On the navigation bar, click on **Instances**

- **Add filter** for `NFS` to see your running instance, and then **right click**, choose **Connect** and for the connection method pick **Session Manager**, then click on **Connect**.

- On the new SSH Session opened in new tab, install **s3fs-fuse** by typing this command

```
$ sudo su
# amazon-linux-extras install epel -y
# yum install s3fs-fuse -y
```

> You can check if s3fs is installed by typing `$ which s3fs` and it should return the directories in which you install s3fs

- Now create a directory or provide the path of an existing directory and mount S3bucket in it by typing:

```
# mkdir -p ~/s3fs-demolab
```

- Now mount the folder you created with your S3 bucket by typing this command

> Please change `nfslab-stack-s3fs-{your.name}` to the name of your S3 Bucket

```
# s3fs nfslab-stack-s3fs-{your.name} ~/s3fs-demolab \
-o iam_role="s3fs-ec2" -o url="https://s3.us-east-1.amazonaws.com" \
-o endpoint=us-east-1 \
-o dbglevel=info \
-o curldbg \
-o use_cache=/tmp \
-f

```

- Check if the bucket has been successfully mounted to your instance

```
# df -h
```

The results would show an s3fs file system mounted with size of 256 Terabytes on the instance

<p align="center">
    <img src="./images/13.png" width=70% height=70%>
</p>


### Test your mounted S3 Bucket

- Now still in the same instance, create a file by typing

```
# cd ~/s3fs-demolab
# touch go.txt
```

- Check if the file go.txt exists in your S3 Bucket

<p align="center">
    <img src="./images/14.png" width=70% height=70%>
</p>


---

<a name="goofys"></a>
### 2. Mount your S3 (goofys)


### Create Another S3 bucket
- On the **service** menu, click **S3**.

- Click **Create Bucket**.

- For Bucket Name, type a **nfslab-stack-goofys-your-name**.

> Ex: `nfslab-stack-goofys-alvin`.

- For Region, choose **US East(N.Virginia)**.

<p align="center">
    <img src="./images/9.png" width=70% height=70%>
</p>


- Scroll down and click on **Create Bucket**

### Create an IAM Role to access the S3 Bucket

- On the **Service** menu, select **IAM**.

- In the navigation pane, click **Roles**.

- Click **Create policy**, then click on the **JSON** tab

- Copy and paste the IAM Policy JSON attached on [IAMPOLICY.json](./materials/IAMPOLICY.json)
> change the `{bucket-name}` part to your **nfslab-stack-goofys-your-name** (S3 Bucket name).


<p align="center">
    <img src="./images/16.png" width=70% height=70%>
</p>

- Then, click on review, type in `goofys-access` as the name of policy then add `Access to S3 Bucket` for the description and them click on **Create Policy**

<p align="center">
    <img src="./images/18.png" width=70% height=70%>
</p>


- Then in the navigation pane, click **Roles**, then click on the roles we created before for s3fs

- Click on **Attach Policy**, tick on **goofys-access** and click on **Attach Policy**

<p align="center">
    <img src="./images/17.png" width=70% height=70%>
</p>


### Mounting your S3 Bucket to EC2

- On the **Service** menu, select **EC2**.

- On the navigation bar, click on **Instances**

- **Add filter** for `NFS` to see your running instance, and then **right click**, choose **Connect** and for the connection method pick **Session Manager**, then click on **Connect**.

- On the new SSH Session opened in new tab, install **goofys-fuse** by typing this command

```
$ sudo su
# yum install golang git fuse -y
# go get github.com/kahing/goofys
(wait about 5 minutes)
# go install github.com/kahing/goofys
# sudo cp ~/go/bin/goofys /usr/bin/
```

> You can check if goofys is installed by typing `$ which goofys` and it should return the directories in which you install goofys

- Now exit from root and create a directory or provide the path of an existing directory and mount S3bucket in it by typing:

```
# exit
$ mkdir -p ~/goofys-demolab
```

- Now mount the folder you created with your S3 bucket by typing this command

> Please change `nfslab-stack-goofys-{your.name}` to the name of your S3 Bucket

```
$ goofys \
-o nonempty \
--region us-east-1 \
--uid=0 \
--gid=0 \
--dir-mode=0777 \
--file-mode=0777 \
nfslab-stack-goofys-{your.name} ~/goofys-demolab

```

- Check if the bucket has been successfully mounted to your instance

```
$ df -h
```

The results would show a goofys file system mounted with size of 1 Petabytes on the instance

<p align="center">
    <img src="./images/15.png" width=70% height=70%>
</p>


### Test your mounted S3 Bucket

- Now still in the same instance, create a file by typing

```
$ cd ~/goofys-demolab
$ touch go.txt
```

- Check if the file go.txt exists in your S3 Bucket

<p align="center">
    <img src="./images/14.png" width=70% height=70%>
</p>

<a name="analysis"></a>
### S3FS vs Goofys Analysis

So after you installing them and tested them both, you would wonder, what is the differences of them?

Well here is the differences:

- The language it was created, which is C for s3fs and Golang for goofys.

<p align="center">
    <img src="./images/41.png" width=70% height=70%>
</p>

- Here a benchtest results between goofys & s3fs which shows that goofys proves to be better at performance when compared to s3fs as it performs quicker

<p align="center">
    <img src="./images/42.png" width=70% height=70%>
</p>

[src](https://github.com/kahing/goofys)

- On the other hand, S3FS proves to support more POSIX System than goofys. [src](https://github.com/s3fs-fuse/s3fs-fuse)

### Implementation

[Click Here to Continue to the Implementation Tutorial](implement.md)

## Conclusion

After finishing this lab, you should had learned what s3fs & goofys are, when to use each of them. Also how to install them and mount it with your S3 Buckets.

## Clean up

* **Cloudformation stack**
* **EC2 instance**
* **S3 Bucket**

## References

* https://github.com/s3fs-fuse/s3fs-fuse
* https://github.com/s3fs-fuse/s3fs-fuse/wiki/FAQ
* https://cloud.ibm.com/docs/services/cloud-object-storage/cli?topic=cloud-object-storage-s3fs
* https://medium.com/tensult/aws-how-to-mount-s3-bucket-using-iam-role-on-ec2-linux-instance-ad2afd4513ef
* https://itneko.com/linux-goofys-s3/
* https://github.com/kahing/goofys/issues/76
<!-- 
 https://aws.amazon.com/storagegateway/
 https://aws.amazon.com/storagegateway/pricing/
 https://medium.com/tensult/creating-aws-file-gateway-as-an-nfs-storage-for-your-ec2-instance-57c141c76409
-->
* https://aws.amazon.com/s3/pricing/
* https://dzone.com/articles/practical-guide-setup-s3-upload-for-wordpress-with