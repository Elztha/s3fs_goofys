# Migrating Static Media Files to S3 using goofys

<p align="center">
    <img src="./Diagram.png" width=70% height=70%>
</p>

## Prerequisites

> Created the NFS Tutorial Cloudformation Stack on [Easy Using S3 with S3FS & Goofys Tutorial](README.md#s3fs)
 

## Step by step

### Setting up your VPC & EC2 Environtment

> ***Important***

> We will make our Environment on the **N. Virginia** Region so make sure that you are already at the **N. Virginia** Region

> If you had done Tutorial **Easy Using S3 with S3FS & Goofys** you can skip to [here](#skip)


- On the **Service** menu, select [CloudFormation](https://console.aws.amazon.com/cloudformation/).

- Select **Create Stack**.

- ☑ **Template is ready**.

- In **Specify template**, ☑ **Upload a template file**.

- Select **Choose file**, and select **[NFSTutorial.yaml](./materials/NFSTutorial.yaml)** and click **Next**.


<p align="center">
    <img src="./images/1.png" width=70% height=70%>
</p>


- Input `NFSLab-Stack-1-your-name` for the **Stack name**

> Ex: `NFSLab-Stack-1-John`.

<p align="center">
    <img src="./images/2.png" width=70% height=70%>
</p>

- Choose the key file that you will use for the EC2 Instance & select **Next**.

- Leave the settings as default, select **Next**.

- Review the details of this stack, if there is no problem select **Create stack**.

### Create S3 bucket
- On the **service** menu, click **S3**.

- Click **Create Bucket**.

- For Bucket Name, type a **nfslab-stack-s3fs-your-name**.

> Ex: `nfslab-stack-goofys-alvin`.

- For Region, choose **US East(N.Virginia)**.

<p align="center">
    <img src="./images/9.png" width=70% height=70%>
</p>


- Scroll down and click on **Create Bucket**

### Create an IAM Role to access the S3 Bucket

- On the **Service** menu, select **IAM**.

- In the navigation pane, click **Roles**.

- Click **Create policy**, then click on the **JSON** tab

- Copy and paste the IAM Policy JSON attached on [IAMPOLICY.json](./materials/IAMPOLICY.json)
> change the `{bucket-name}` part to your **nfslab-stack-goofys-your-name** (S3 Bucket name).


<p align="center">
    <img src="./images/16.png" width=70% height=70%>
</p>

- Then, click on review, type in `s3-access` as the name of policy then add `Access to S3 Bucket` for the description and them click on **Create Policy**

<p align="center">
    <img src="./images/19.png" width=70% height=70%>
</p>

--

- Then in the navigation pane, click **Roles**, then click on **Create Roles**

- Pick **EC2** as the use case then click on **Next:Permissions**
<p align="center">
    <img src="./images/6.png" width=70% height=70%>
</p>

- On the **Filter Policies**, search for the **s3-access** & **AmazonSSMManagedInstanceCore** policy we just created and tick on it to attach

<p align="center">
    <img src="./images/20.png" width=70% height=70%>
</p>

<p align="center">
    <img src="./images/12.png" width=70% height=70%>
</p>

- Click on **Next:tags**, then click again on **Next:Review** and input your role name as `s3-ec2` and then click on **Create Role**

<p align="center">
    <img src="./images/21.png" width=70% height=70%>
</p>


### Attaching IAM Role to your EC2

- On the **Service** menu, select **EC2**.

- On the navigation bar, click on **Instances**

- **Add filter** for `NFS` to see your running instance, and then **right click**, choose **Instance Settings**, and then **Attach/Replace IAM Roles**

<p align="center">
    <img src="./images/10.png" width=70% height=70%>
</p>

- Choose the IAM role we created before (**s3-ec2**), then click **Apply**


### Installing goofys on EC2
- On the **Service** menu, select **EC2**.

- On the navigation bar, click on **Instances**

- **Add filter** for `NFS` to see your running instance, and then **right click**, choose **Connect** and for the connection method pick **Session Manager**, then click on **Connect**.

- On the new SSH Session opened in new tab, install **goofys** by typing this command

```
$ sudo su
# yum install golang git fuse -y
# go get github.com/kahing/goofys
(wait about 5 minutes)
# go install github.com/kahing/goofys
# sudo cp ~/go/bin/goofys /usr/bin/
```

> You can check if goofys is installed by typing `$ which goofys` and it should return the directories in which you install goofys

<a name="skip"></a>

### Installing wordpress on EC2

- On the same EC2 Instance, install **Wordpress** by typing this command

```
# amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2
# sudo yum install -y httpd mariadb-server
# cd /var/www/html
# wget http://wordpress.org/latest.tar.gz
# tar -xzvf latest.tar.gz
# sudo service httpd start
# sudo service mariadb start
```
- Create a new database for the wordpress by typing

```
# mysql -u root -p 
```
> click on enter button as there is no password then input the sql :

```
CREATE USER 'wordpress-user'@'localhost' IDENTIFIED BY 'your_strong_password';
CREATE DATABASE `wordpress-db`;
GRANT ALL PRIVILEGES ON `wordpress-db`.* TO "wordpress-user"@"localhost";
FLUSH PRIVILEGES;
exit
```

- Restart your database

```
# service mariadb restart
```

- Now that Apache & Mysql have started, On your browser navigate to the `<your-ec2-address-hostname>/wordpress`

<p align="center">
    <img src="./images/22.png" width=70% height=70%>
</p>

- Type in the wordpress database connection as below :

<p align="center">
    <img src="./images/23.png" width=70% height=70%>
</p>

- Click on **Run the Installation** and then setup your site name (this contains, site-title, wordpress username and password )

<p align="center">
    <img src="./images/24.png" width=70% height=70%>
</p>

- Click on **Install Wordpress**, then **Login**
- Type in your wordpress username and password, then click on **Login**

<p align="center">
    <img src="./images/25.png" width=50% height=70%>
</p>

- You will be taken to the dashboard of your wordpress page

### Updating your S3 Bucket to Enable Public Access

- On the **service** menu, click **S3**.

- Find and Click your bucket, and then pick permissions tab, and then edit the public access by **untick Block all public access** and then click save.

<p align="center">
    <img src="./images/26.png" width=50% height=70%>
</p>

- Click on Bucket Policy Button, and then copy and paste the [Bucket Policy](./materials/BucketPolicy.json)

> Don't Forget to change `{bucket-name}` to your real bucket name

<p align="center">
    <img src="./images/27.png" width=50% height=70%>
</p>

### Mounting Wordpress Uploads Folder to S3 using Goofys

- Before mounting, stop your httpd server running 

```
# service httpd stop
```

- Mount the uploads folder of wordpress to your S3 bucket by typing this command

> Please change `nfslab-stack-goofys-{your.name}` to the name of your S3 Bucket

```
$ goofys \
-o allow_other \
--region us-east-1 \
--uid=0 \
--gid=0 \
--dir-mode=0777 \
--file-mode=0777 \
nfslab-stack-goofys-alvin /var/www/html/wordpress/wp-content/uploads

```

- Update your permission for wordpress file uploads

```
# exit
$ sudo usermod -a -G apache ec2-user
$ chown -R ec2-user:apache /var/www
$ chmod 2775 /var/www && find /var/www -type d -exec sudo chmod 2775 {} \;
$ find /var/www -type f -exec sudo chmod 0664 {} \;
# sudo su
```

- Change all your media uploads link to your s3 link by 

```
# vim /var/www/html/wordpress/wp-content/themes/twentytwenty/functions.php
```
then add the line : 
> Change {your-s3-bucket-name} to your real s3 bucket name

```
add_filter( 'pre_option_upload_url_path', 'upload_url' );

function upload_url() {
    return 'https://{your-s3-bucket-name}.s3.amazonaws.com';
}
```

- Start your httpd server

```
# service httpd start
```

### Try Uploading Stuff

On your browser navigate to the `<your-ec2-address-hostname>/wordpress`

<p align="center">
    <img src="./images/22.png" width=70% height=70%>
</p>

Click on **Media** Bar on the left

<p align="center">
    <img src="./images/28.png" width=30% height=70%>
</p>

Upload any image by clicking **Select Files**

<p align="center">
    <img src="./images/30.png" width=50% height=70%>
</p>

And then click on your picture after it finished uploading

<p align="center">
    <img src="./images/31.png" width=50% height=70%>
</p>


You will notice that your page serves all Uploaded Media through your S3 Bucket which increases your website speed as static files are hosted from S3.

<p align="center">
    <img src="./images/32.png" width=80% height=70%>
</p>


## Conclusion

Congratulations you have learned how to create a Wordpress Site with EC2 through Cloudformation, and migrate all your wordpress media uploads data to S3 by mounting it. Don't forget to clean up resources if you are just testing.

### Challenge

*But Can you do it with s3fs tho?*

## Clean up 

* **Cloudformation stack**
* **S3 Bucket**
* **IAM Roles & Policy**
